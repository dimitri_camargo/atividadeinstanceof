/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overloading;

/**
 *
 * @author aluno.redes
 */
public class AdvancedCalculator extends Calculator{
   
    public  static final int MAX_USER = 1;
    
    public AdvancedCalculator(){
        
    }
    
    public AdvancedCalculator(double fNum1, double fNum2){
        super.fNumber1 = fNum1;
        super.fNumber2 = fNum2;
    }
    
    public static double SquareRoot(double fNum1){
        return Math.sqrt(fNum1);        
    }
    
    //exponentiation methods
    public double exponentiation(){
        return Math.pow(fNumber1, fNumber1);
    }
    
    public static double exponentiation(double fNum1, double fNum2){
        return Math.pow(fNum2, fNum2);
    }
    
    public static double sine(double fNum){
        return Math.sin(fNum);
    }
    
    public static double cosine(double fNum){
        return Math.cos(fNum);
    }
    
    public static double tangent(double fNum){
        return Math.tan(fNum);
    }
}
