/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overloading;

/**
 *
 * @author aluno.redes
 */
public class Calculator {
    protected double fNumber1;
    protected double fNumber2;
    
    //class constructor 
    public Calculator(){
        //default values
        this.fNumber1 = 0;
        this.fNumber2 = 0;
    }
    
    //overloading constructor
    public Calculator(double fnumber1, double fNumber2){
        this.fNumber1 = fNumber1;
        this.fNumber2 = fNumber2;
    }

    public double getfNumber1() {
        return fNumber1;
    }

    public void setfNumber1(double fNumber1) {
        this.fNumber1 = fNumber1;
    }

    public double getfNumber2() {
        return fNumber2;
    }

    public void setfNumber2(double fNumber2) {
        this.fNumber2 = fNumber2;
    }
    
    //methods Overloading
    public double Sum(){
        System.out.println("sum()");
        return this.fNumber1 + this.fNumber2;
    }
    
    public static int Sum(int Number1, int Number2){
        System.out.println("sum(int, int)");
        return Number1 + Number2;
    }
    
    public static double Sum(double Number1, double Number2){
        System.out.println("sum(double, double)");
        return Number1+Number2;
    }
    
    //subtraction methods
    public double subtraction(){
        return this.fNumber1 - this.fNumber2;
    }
    
    public static double subtraction(double fNum1, double fNum2){
        return fNum1 - fNum2;
    }
    
    //multiplication methods
    public double multiplication(){
        return this.fNumber1 * this.fNumber2;
    }
    
    public static double multiplication(double fNum1, double fNum2){
        return fNum1 * fNum2;
    }
    
    //division methods
    public double division(){
        return this.fNumber1 / this.fNumber2;
    }
    
    public static double division(double fNum1, double fNum2){
        return fNum1 / fNum2;
    }
    
    
    
}
